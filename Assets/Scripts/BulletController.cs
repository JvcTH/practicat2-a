using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    

    private Rigidbody2D rb;
    private GameController _game;
    
    private SpriteRenderer sr;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        _game = FindObjectOfType<GameController>();
        
        sr = GetComponent<SpriteRenderer>();
        Destroy(this.gameObject, 3);
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.flipX)
        {
            rb.velocity = new Vector2(-6, rb.velocity.y);
        }else
        {
            rb.velocity = new Vector2(6, rb.velocity.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        

        if (other.gameObject.CompareTag("enemy"))
        {
            Destroy(other.gameObject);
            _game.PlusScore(10);
        }
        
    }
}

