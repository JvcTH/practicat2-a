using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour
{
    public Text scoreText;
    public Text tipo1Text;
    public Text tipo2Text;
    public Text tipo3Text;
    
    
    private int _score = 0;
    private int _tipo1 = 0;
    private int _tipo2 = 0;
    private int _tipo3 = 0;
    

    private void Start()
    {
        scoreText.text = "Score: " + _score;
        tipo1Text.text = "Moneda Tipo 1: " + _tipo1;
        tipo2Text.text = "Moneda Tipo 2: " + _tipo2;
        tipo3Text.text = "Moneda Tipo 3: " + _tipo2;
    }


    public void PlusScore(int score)
    {
        _score += score;
        scoreText.text = "Score: " + _score;
    }

    public void PlusTipo1(int tipo1)
    {
        _tipo1 += tipo1;
        tipo1Text.text = "Moneda Tipo 1: " + _tipo1;
    }


    public void PlusTipo2(int tipo2)
    {
        _tipo2 += tipo2;
        tipo2Text.text = "Moneda Tipo 2: " + _tipo2;
    }


    public void PlusTipo3(int tipo3)
    {
        _tipo3 += tipo3;
        tipo3Text.text = "Moneda Tipo 3: " + _tipo2;
    }


    
}